package types

import (
	"database/sql"
)

type NullString sql.NullString

// MarshalTo exists to fit gogoprotobuf custom type interface.
func (s NullString) MarshalTo(data []byte) (n int, err error) {
	if !s.Valid {
		return 0, nil
	}
	copy(data, []byte(s.String))
	return len(s.String), nil
}

// Size exists to fit gogoprotobuf custom type interface.
func (s *NullString) Size() int {
	if !s.Valid {
		return 0
	}
	return len(s.String)
}

// Unmarshal exists to fit gogoprotobuf custom type interface.
func (s *NullString) Unmarshal(data []byte) error {
	if len(data) == 0 {
		s = nil
		return nil
	}
	s.String = string(data)
	s.Valid = true
	return nil
}

