package types

import (
	"bytes"
	"database/sql/driver"
	"encoding/json"
	"errors"
	"fmt"
	"log"
)

type Jsonb []byte

// Marshal encodes Raw to slice of bytes. Exists to fit gogoprotobuf custom
// type interface.
func (r Jsonb) Marshal() ([]byte, error) {
	if len(r) == 0 {
		return nil, nil
	}
	return []byte(r), nil
}

// MarshalTo exists to fit gogoprotobuf custom type interface.
func (r Jsonb) MarshalTo(data []byte) (n int, err error) {
	if len(r) == 0 {
		return 0, nil
	}
	copy(data, r)
	return len(r), nil
}

// Unmarshal exists to fit gogoprotobuf custom type interface.
func (r *Jsonb) Unmarshal(data []byte) error {
	if len(data) == 0 {
		r = nil
		return nil
	}
	id := Jsonb(make([]byte, len(data)))
	copy(id, data)
	*r = id
	return nil
}

// Size exists to fit gogoprotobuf custom type interface.
func (r *Jsonb) Size() int {
	if r == nil {
		return 0
	}
	return len(*r)
}

// MarshalJSON returns *r as the JSON encoding of r.
func (r Jsonb) MarshalJSON() ([]byte, error) {
	if r == nil {
		return []byte("null"), nil
	}
	return r, nil
}

// UnmarshalJSON sets *r to a copy of data.
func (r *Jsonb) UnmarshalJSON(data []byte) error {
	if r == nil {
		return errors.New("Raw: UnmarshalJSON on nil pointer")
	}
	*r = append((*r)[0:0], data...)
	return nil
}

// Equal exists to fit gogoprotobuf custom type interface.
func (r Jsonb) Equal(other Jsonb) bool {
	return bytes.Equal(r[0:], other[0:])
}

// Compare exists to fit gogoprotobuf custom type interface.
func (r Jsonb) Compare(other Jsonb) int {
	return bytes.Compare(r[0:], other[0:])
}

type intn interface {
	Intn(n int) int
}

// NewPopulatedRaw required for gogoprotobuf custom type.
func NewPopulatedRaw(r intn) *Jsonb {
	v1 := r.Intn(100)
	data := make([]byte, v1)
	for i := 0; i < v1; i++ {
		data[i] = byte('a')
	}
	d := `{"key":"` + string(data) + `"}`
	raw := Jsonb([]byte(d))
	return &raw
}

// Value get value of Jsonb
func (r Jsonb) Value() (driver.Value, error) {
	if len(r) == 0 {
		return nil, nil
	}
	return r.MarshalJSON()
}

// Scan scan value into Jsonb
func (r *Jsonb) Scan(value interface{}) error {
	var bytes []byte

	switch v := value.(type) {
	case []byte:
		bytes = v
	case map[string]interface{}:
		jsonBytes, err := json.Marshal(v)
		if err != nil {
			return errors.New(fmt.Sprint("Failed to unmarshal JSONB value:", value))
		}
		bytes = jsonBytes
	default:
		return errors.New(fmt.Sprint("Failed to unmarshal JSONB value:", value))
	}

	return json.Unmarshal(bytes, r)
}

func ToJsonb(value interface{}) (*Jsonb, error) {
	bytes, err := json.Marshal(value)
	if err != nil {
		return nil, err
	}
	jsonb := Jsonb(bytes)
	return &jsonb, nil
}

func MustJsonb(value interface{}) *Jsonb {
	bytes, err := json.Marshal(value)
	if err != nil {
		log.Fatal(err)
	}
	jsonb := Jsonb(bytes)
	return &jsonb
}
