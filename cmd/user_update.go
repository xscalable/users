package cmd

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/xscalable/users/client"
	pb "gitlab.com/xscalable/users/proto"
	"gitlab.com/xscalable/users/types"
)

var updateUserCmd = &cobra.Command{
	Use:   "update",
	Short: "update",
	Long:  `Update an user`,
	Run: func(cmd *cobra.Command, args []string) {
		uid := viper.GetString("update_user.uid")
		email := viper.GetString("update_user.email")
		password := viper.GetString("update_user.password")
		name := viper.GetString("update_user.name")
		undelete := viper.GetBool("update_user.undelete")
		server := viper.GetString("update_user.server")

		user := &pb.User{Email: email, Password: password, Name: name}
		if undelete {
			user.DeletedAt = &types.NullTime{}
		}
		client.UpdateUser(server, uid, user)
	},
}

func init() {
	rootCmd.AddCommand(updateUserCmd)

	updateUserCmd.Flags().StringP("server", "s", "localhost:50051", "remote server address")
	updateUserCmd.Flags().StringP("uid", "u", "", "user id")
	updateUserCmd.Flags().StringP("email", "e", "", "user email")
	updateUserCmd.Flags().StringP("password", "p", "", "user password")
	updateUserCmd.Flags().StringP("name", "n", "", "user name")
	updateUserCmd.Flags().BoolP("undelete", "", false, "undelete user")

	viper.BindPFlag("update_user.server", updateUserCmd.Flags().Lookup("server"))
	viper.BindPFlag("update_user.email", updateUserCmd.Flags().Lookup("email"))
	viper.BindPFlag("update_user.password", updateUserCmd.Flags().Lookup("password"))
	viper.BindPFlag("update_user.name", updateUserCmd.Flags().Lookup("name"))
	viper.BindPFlag("update_user.uid", updateUserCmd.Flags().Lookup("uid"))
	viper.BindPFlag("update_user.undelete", updateUserCmd.Flags().Lookup("undelete"))

}
