package cmd

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/xscalable/users/client"
	pb "gitlab.com/xscalable/users/proto"
)

var createUserCmd = &cobra.Command{
	Use:   "create",
	Short: "create",
	Long:  `Create an user`,
	Run: func(cmd *cobra.Command, args []string) {
		email := viper.GetString("create_user.email")
		password := viper.GetString("create_user.password")
		server := viper.GetString("create_user.server")
		user := &pb.User{Email: email, Password: password}
		client.CreateUser(server, user)
	},
}

func init() {
	rootCmd.AddCommand(createUserCmd)

	createUserCmd.Flags().StringP("server", "s", "localhost:50051", "remote server address")
	createUserCmd.Flags().StringP("email", "e", "pauloramires@gmail.com", "user email")
	createUserCmd.Flags().StringP("password", "p", "secret", "user password")

	viper.BindPFlag("create_user.server", createUserCmd.Flags().Lookup("server"))
	viper.BindPFlag("create_user.email", createUserCmd.Flags().Lookup("email"))
	viper.BindPFlag("create_user.password", createUserCmd.Flags().Lookup("password"))
}
