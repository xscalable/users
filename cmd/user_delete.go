package cmd

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/xscalable/users/client"
)

var deleteUserCmd = &cobra.Command{
	Use:   "delete",
	Short: "delete",
	Long:  `Delete an user`,
	Run: func(cmd *cobra.Command, args []string) {
		uid := viper.GetString("delete_user.uid")
		server := viper.GetString("delete_user.server")
		client.DeleteUser(server, uid)
	},
}

func init() {
	rootCmd.AddCommand(deleteUserCmd)

	deleteUserCmd.Flags().StringP("server", "s", "localhost:50051", "remote server address")
	deleteUserCmd.Flags().StringP("uid", "u", "", "user uid")

	viper.BindPFlag("delete_user.server", deleteUserCmd.Flags().Lookup("server"))
	viper.BindPFlag("delete_user.uid", deleteUserCmd.Flags().Lookup("uid"))
}
