package cmd

import (
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/xscalable/users/client"
)

var listUsersCmd = &cobra.Command{
	Use:   "list",
	Short: "list",
	Long:  `List users`,
	Run: func(cmd *cobra.Command, args []string) {
		log.Infof("LISTING USERS")
		limit := viper.GetInt32("list_users.limit")
		offset := viper.GetInt32("list_users.offset")
		deleted := viper.GetBool("list_users.deleted")
		server := viper.GetString("list_users.server")
		client.ListUsers(server, limit, offset, deleted)
	},
}

func init() {
	rootCmd.AddCommand(listUsersCmd)

	listUsersCmd.Flags().StringP("server", "s", "localhost:50051", "remote server address")
	listUsersCmd.Flags().Int32P("limit", "l", -1, "limit")
	listUsersCmd.Flags().Int32P("offset", "o", -1, "offset")
	listUsersCmd.Flags().BoolP("deleted", "d", false, "show deleted records")

	viper.BindPFlag("list_users.server", listUsersCmd.Flags().Lookup("server"))
	viper.BindPFlag("list_users.limit", listUsersCmd.Flags().Lookup("limit"))
	viper.BindPFlag("list_users.offset", listUsersCmd.Flags().Lookup("offset"))
	viper.BindPFlag("list_users.deleted", listUsersCmd.Flags().Lookup("deleted"))
}
