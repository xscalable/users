package cmd

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/xscalable/users/client"
)

var loginCmd = &cobra.Command{
	Use:   "login",
	Short: "login",
	Long:  `Login with user credentials`,
	Run: func(cmd *cobra.Command, args []string) {
		email := viper.GetString("email")
		password := viper.GetString("password")
		server := viper.GetString("server")
		client.CheckCredentials(server, email, password)
	},
}

func init() {
	rootCmd.AddCommand(loginCmd)

	loginCmd.Flags().StringP("server", "s", "localhost:50051", "remote server address")
	loginCmd.Flags().StringP("email", "e", "pauloramires@gmail.com", "user email")
	loginCmd.Flags().StringP("password", "p", "secret", "user password")

	viper.BindPFlag("server", loginCmd.Flags().Lookup("server"))
	viper.BindPFlag("email", loginCmd.Flags().Lookup("email"))
	viper.BindPFlag("password", loginCmd.Flags().Lookup("password"))
}
