package cmd

import (
	"bytes"
	"fmt"
	"os"

	"github.com/dimiro1/banner"
	"github.com/gobuffalo/packr"
	colorable "github.com/mattn/go-colorable"
	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/xscalable/users/server"
)

var cfgFile string

var rootCmd = &cobra.Command{
	Use:   "users",
	Short: "xscalable users service",
	Long:  `xScalable Users Service`,
	Run: func(cmd *cobra.Command, args []string) {
		config := server.Config{
			Environment: viper.GetString("environment"),
			Buildstamp:  viper.GetString("buildstamp"),
			Githash:     viper.GetString("githash"),
			Version:     viper.GetString("version"),
			GRPCAddr:    viper.GetString("grpc.addr"),
			DBConn:      viper.GetString("postgres.conn"),
		}
		server.Start(config)
	},
}

func showBanner() {
	box := packr.NewBox("../resources")
	bannerString, _ := box.FindString("banner.txt")
	banner.Init(colorable.NewColorableStdout(), true, true, bytes.NewBufferString(bannerString))
}

// Execute rootCmd
func Execute() {
	showBanner()
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.users.yml)")

	rootCmd.PersistentFlags().StringP("grpc_addr", "", ":50051", "listening address for gRPC server")
	rootCmd.Flags().StringP("postgres_conn", "", "host=127.0.0.1 port=5432 user=usermanager dbname=xscalable_users password=secret sslmode=disable", "postgres connection")

	viper.BindPFlag("grpc.addr", rootCmd.PersistentFlags().Lookup("grpc_addr"))
	viper.BindPFlag("postgres.conn", rootCmd.Flags().Lookup("postgres_conn"))
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		// Search config in both current and home directory with name ".users" (without extension).
		viper.SetConfigName(".xscalable_users")
		viper.AddConfigPath(".")
		viper.AddConfigPath(home)
	}

	viper.AutomaticEnv() // read in environment variables that match
	viper.SetEnvPrefix("xscalable_users")

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}
}
