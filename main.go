//go:generate protoc -I=. -I=./vendor --gogofaster_out=plugins=grpc:. proto/users.proto
//go:generate protoc-go-inject-tag -input=proto/users.pb.go

package main

import (
	"os"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/xscalable/users/cmd"
)

var (
	version     = "development"
	buildstamp  = ""
	githash     = "local"
	environment = ""
)

func main() {
	if len(buildstamp) == 0 {
		buildstamp = time.Now().Format(time.RFC3339)
	}
	if len(environment) == 0 {
		if env := os.Getenv("XSCALABLE_USERS_ENVIRONMENT"); len(env) > 0 && env != "production" {
			environment = env
			log.SetFormatter(&log.TextFormatter{})
		} else {
			environment = "production"
			log.SetFormatter(&log.JSONFormatter{})
		}
	}
	os.Setenv("XSCALABLE_USERS_ENVIRONMENT", environment)
	os.Setenv("XSCALABLE_USERS_BUILDSTAMP", buildstamp)
	os.Setenv("XSCALABLE_USERS_GITHASH", githash)
	os.Setenv("XSCALABLE_USERS_VERSION", version)

	cmd.Execute()
}
