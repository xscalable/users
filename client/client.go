package client

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"time"

	log "github.com/sirupsen/logrus"
	pb "gitlab.com/xscalable/users/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const (
	address     = "localhost:50051"
	defaultName = "world"
)

// Start client
func Start() {
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := pb.NewUsersClient(conn)

	// Contact the server and print out its response.
	name := defaultName
	if len(os.Args) > 1 {
		name = os.Args[1]
	}
	_ = name
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	r, err := c.Health(ctx, &pb.EmptyRequest{Handler: 123})
	if err != nil {
		log.Fatalf("could not greet: %v", err)
	}
	log.Infof("Greeting: %v", r.Handler)
}

// CheckCredentials validates user credentials remotely
func CheckCredentials(server string, email string, password string) {
	fmt.Printf("checking credentials@%s\n", server)
	conn, err := grpc.Dial(server, grpc.WithInsecure())
	if err != nil {
		st, ok := status.FromError(err)
		if !ok {
			// Error was not a status error
			log.Fatalf("did not connect: %v", err)
		}
		log.Error(st.Err())
	}
	defer conn.Close()
	c := pb.NewUsersClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	user, err := c.CheckCredentials(ctx, &pb.LoginRequest{Email: email, Password: password})
	if err != nil {
		st, ok := status.FromError(err)
		if !ok {
			log.Fatalf("unknown error: %v", err)
		}
		if st.Code() == codes.NotFound {
			log.Warn("invalid username or password, check your credentials")
		} else {
			log.Error(st.Err())
		}
		return
	}
	userJSON, err := json.MarshalIndent(user, "", "  ")
	if err != nil {
		log.Fatalf("error while marshaling response into json: %v", err)
	}
	fmt.Println(string(userJSON))
}

// CreateUser creates an user in the database
func CreateUser(server string, user *pb.User) {
	fmt.Printf("creating user %s@%s\n", user.Email, server)
	conn, err := grpc.Dial(server, grpc.WithInsecure())
	if err != nil {
		st, ok := status.FromError(err)
		if !ok {
			// Error was not a status error
			log.Fatalf("did not connect: %v", err)
		}
		log.Error(st.Err())
	}
	defer conn.Close()
	c := pb.NewUsersClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	_user, err := c.CreateUser(ctx, user)
	if err != nil {
		st, ok := status.FromError(err)
		if !ok {
			log.Fatalf("unknown error: %v", err)
		}
		if st.Code() == codes.NotFound {
			log.Warn("invalid username or password, check your credentials")
		} else {
			log.Error(st.Err())
		}
		return
	}
	userJSON, err := json.MarshalIndent(_user, "", "  ")
	if err != nil {
		log.Fatalf("error while marshaling response into json: %v", err)
	}
	fmt.Println(string(userJSON))
}

// UpdateUser updates an user in the database
func UpdateUser(server string, uid string, user *pb.User) {
	fmt.Printf("updating user %s@%s\n", user.Email, server)
	conn, err := grpc.Dial(server, grpc.WithInsecure())
	if err != nil {
		st, ok := status.FromError(err)
		if !ok {
			// Error was not a status error
			log.Fatalf("did not connect: %v", err)
		}
		log.Error(st.Err())
	}
	defer conn.Close()
	c := pb.NewUsersClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	req := &pb.UpdateUserRequest{Uid: uid, User: user}

	_user, err := c.UpdateUser(ctx, req)
	if err != nil {
		st, ok := status.FromError(err)
		if !ok {
			log.Fatalf("unknown error: %v", err)
		}
		if st.Code() == codes.NotFound {
			log.Errorf("user %s not found", uid)
		} else {
			log.Error(st.Err())
		}
		return
	}
	userJSON, err := json.MarshalIndent(_user, "", "  ")
	if err != nil {
		log.Fatalf("error while marshaling response into json: %v", err)
	}
	fmt.Println(string(userJSON))
}

// DeleteUser deletes an user
func DeleteUser(server string, uid string) {
	fmt.Printf("updating user %s@%s\n", uid, server)
	conn, err := grpc.Dial(server, grpc.WithInsecure())
	if err != nil {
		st, ok := status.FromError(err)
		if !ok {
			// Error was not a status error
			log.Fatalf("did not connect: %v", err)
		}
		log.Error(st.Err())
	}
	defer conn.Close()
	c := pb.NewUsersClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	_user, err := c.DeleteUser(ctx, &pb.DeleteUserRequest{Uid: uid})
	if err != nil {
		st, ok := status.FromError(err)
		if !ok {
			log.Fatalf("unknown error: %v", err)
		}
		if st.Code() == codes.NotFound {
			log.Errorf("user %s not found", uid)
		} else {
			log.Error(st.Err())
		}
		return
	}
	userJSON, err := json.MarshalIndent(_user, "", "  ")
	if err != nil {
		log.Fatalf("error while marshaling response into json: %v", err)
	}
	fmt.Println(string(userJSON))
}

// ListUsers list users
func ListUsers(server string, limit int32, offset int32, deleted bool) {
	conn, err := grpc.Dial(server, grpc.WithInsecure())
	if err != nil {
		st, ok := status.FromError(err)
		if !ok {
			// Error was not a status error
			log.Fatalf("did not connect: %v", err)
		}
		log.Error(st.Err())
	}
	defer conn.Close()
	c := pb.NewUsersClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	users, err := c.ListUsers(ctx, &pb.ListUsersRequest{Limit: limit, Offset: offset, Deleted: deleted})
	if err != nil {
		st, ok := status.FromError(err)
		if !ok {
			log.Fatalf("unknown error: %v", err)
		}
		log.Error(st.Err())
		return
	}
	usersJSON, err := json.MarshalIndent(users, "", "  ")
	if err != nil {
		log.Fatalf("error while marshaling users into json: %v", err)
	}
	fmt.Println(string(usersJSON))
}
