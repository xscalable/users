package server

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"time"

	"github.com/hashicorp/consul/api"
	consul "github.com/hashicorp/consul/api"
	"github.com/jinzhu/gorm"
	"github.com/lib/pq"
	uuid "github.com/satori/go.uuid"
	log "github.com/sirupsen/logrus"
	pb "gitlab.com/xscalable/users/proto"
	"golang.org/x/crypto/bcrypt"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type UserService struct {
	config      Config
	Name        string
	ID          string
	DB          *gorm.DB
	ConsulAgent *consul.Agent
	TTL         time.Duration
	Stop        chan bool
	checkID     string
}

func (s *UserService) Check() (bool, error) {
	if err := s.DB.DB().Ping(); err != nil {
		log.Errorf("service check error: %v", err)
		return false, err
	}
	return true, nil
}

func (s *UserService) UpdateTTL(check func() (bool, error)) {
	for range time.NewTicker(s.TTL / 2).C {
		s.update(check)
	}
}

func (s *UserService) update(check func() (bool, error)) {
	ok, err := check()
	if !ok {
		log.Errorf("err=\"Check failed\" msg=\"%s\"", err.Error())
		if err := s.ConsulAgent.UpdateTTL(s.checkID, err.Error(), "fail"); err != nil {
			log.Error(err)
		}
		return
	}
	if err := s.ConsulAgent.UpdateTTL(s.checkID, fmt.Sprintf("service %s up and running", s.Name), "pass"); err != nil {
		log.Error(err)
	}
}

func (s *UserService) registerService() error {
	if s.ConsulAgent == nil {
		log.Warn("can't register service, consulAgent not available...")
		return nil
	}

	s.checkID = uuid.NewV4().String()

	serviceDef := &consul.AgentServiceRegistration{
		Name: s.Name,
		ID:   s.ID,
		Tags: []string{"xscalable"},
		Checks: consul.AgentServiceChecks{
			&consul.AgentServiceCheck{
				CheckID: s.checkID,
				TTL:     s.TTL.String(),
			},
		},
	}

	return s.ConsulAgent.ServiceRegister(serviceDef)
}

func (s *UserService) deRegisterService() error {
	if s.ConsulAgent == nil {
		return nil
	}
	return s.ConsulAgent.ServiceDeregister(s.ID)
}

func openDBConnection(config Config) (*gorm.DB, error) {
	db, err := gorm.Open("postgres", config.DBConn)
	if err != nil {
		return nil, err
	}
	if config.Environment == "development" {
		db.LogMode(true)
	}

	db.DB().SetMaxIdleConns(5)
	db.DB().SetMaxOpenConns(20)
	db.DB().SetConnMaxLifetime(time.Minute * 15)

	if err = db.DB().Ping(); err != nil {
		log.Warnf("%v", err)
		log.Warn("retrying database connection in 5 seconds... ")
		time.Sleep(time.Duration(5) * time.Second)
		return openDBConnection(config)
	}
	log.Info("connected to the database")
	return db, nil
}

func (s *UserService) Close() {
	close(s.Stop)
	if err := s.DB.Close(); err != nil {
		log.Warn(err)
	}
	if err := s.deRegisterService(); err != nil {
		log.Warn(err)
	}
	os.Exit(0)
}

func (s *UserService) envIsDevelopment() bool {
	return s.config.Environment == "development"
}

// NewUsersHandler returns a new instance of UserService
func NewUsersHandler(config Config) *UserService {
	gracefulStop := make(chan os.Signal)
	signal.Notify(gracefulStop, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT, syscall.SIGKILL)

	consulClient, err := api.NewClient(api.DefaultConfig())
	if err != nil {
		log.Fatal(err)
	}

	db, err := openDBConnection(config)
	if err != nil {
		log.Fatal(err)
	}

	if err := db.Exec("CREATE EXTENSION IF NOT EXISTS \"uuid-ossp\";").Error; err != nil {
		log.Fatal(err)
	}
	if err := db.AutoMigrate(&pb.User{}).Error; err != nil {
		log.Fatal(err)
	}

	s := &UserService{
		config:      config,
		Name:        "org.xscalable.service.users",
		ID:          uuid.NewV4().String(),
		DB:          db,
		ConsulAgent: consulClient.Agent(),
		TTL:         time.Second * 15,
		Stop:        make(chan bool),
	}

	go func() {
		sig := <-gracefulStop
		log.Debugf("caught sig: %+v", sig)
		s.Close()
	}()

	if err := s.registerService(); err != nil {
		log.Fatal(err)
	}
	go s.UpdateTTL(s.Check)

	return s
}

// Health fetches service status
func (s *UserService) Health(ctx context.Context, req *pb.EmptyRequest) (*pb.EmptyReply, error) {
	/*
		md, ok := metadata.FromContext(ctx)
		if !ok {
			log.Warn("No metadata received")
		} else {
			log.Infof("Received metadata %v\n", md)
			authorization := md["Authorization"]
			fmt.Println("Authorization: " + authorization)
		}
	*/

	return &pb.EmptyReply{Handler: int32(666)}, nil
}

// CheckCredentials verify credentials against database
func (s *UserService) CheckCredentials(ctx context.Context, req *pb.LoginRequest) (*pb.User, error) {
	email := req.GetEmail()
	password := req.GetPassword()

	if len(email) == 0 || len(password) == 0 {
		return nil, status.Error(codes.NotFound, "invalid username or password")
	}
	user := &pb.User{}
	if err := s.DB.Where("email = ?", email).First(user).Error; err != nil {
		return nil, status.Error(codes.NotFound, "invalid username or password")
	}

	if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password)); err != nil {
		return nil, status.Error(codes.NotFound, "invalid username or password")
	}
	return user, nil
}

// CreateUser creates an user
func (s *UserService) CreateUser(ctx context.Context, req *pb.User) (*pb.User, error) {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(req.Password), 14)
	if err != nil {
		return nil, status.Error(codes.Unknown, err.Error())
	}
	req.Password = string(hashedPassword)
	err = s.DB.Create(req).Error
	if err != nil {
		// we got a libpq error
		if pqErr, ok := err.(*pq.Error); ok {
			if pqErr.Message == "duplicate key value violates unique constraint \"uix_users_email\"" {
				return nil, status.Error(codes.AlreadyExists, fmt.Sprintf("email %s is already taken", req.Email))
			}
			return nil, status.Error(codes.Unknown, pqErr.Message)
		}
		return nil, status.Error(codes.Unknown, err.Error())
	}
	return req, nil
}

// GetUser get an user
func (s *UserService) GetUser(ctx context.Context, req *pb.GetUserRequest) (*pb.User, error) {
	user := &pb.User{}
	if err := s.DB.Where("email = ?", req.Email).First(user).Error; err != nil {
		return nil, status.Error(codes.Unknown, err.Error())
	}
	return user, nil
}

// UpdateUser update an user
func (s *UserService) UpdateUser(ctx context.Context, req *pb.UpdateUserRequest) (*pb.User, error) {
	uid := req.GetUid()
	if _, err := uuid.FromString(uid); err != nil {
		return nil, status.Error(codes.NotFound, "user not found")
	}

	user := req.GetUser()
	if len(user.Password) > 0 {
		hashedPassword, err := bcrypt.GenerateFromPassword([]byte(user.Password), 14)
		if err != nil {
			return nil, status.Error(codes.Unknown, err.Error())
		}
		user.Password = string(hashedPassword)
	}
	query := s.DB.Model(&pb.User{})
	if user.DeletedAt != nil && !user.DeletedAt.Valid {
		query = query.Unscoped()
	}

	if err := query.Where("uid = ?", req.GetUid()).Updates(user).Error; err != nil {
		if pqErr, ok := err.(*pq.Error); ok {
			if pqErr.Message == "duplicate key value violates unique constraint \"uix_users_email\"" {
				return nil, status.Error(codes.AlreadyExists, fmt.Sprintf("email %s is already taken", req.User.Email))
			}
			return nil, status.Error(codes.Unknown, pqErr.Message)
		}
		return nil, status.Error(codes.Unknown, err.Error())
	}
	user.Uid = req.GetUid()
	return user, nil
}

// DeleteUser delete an user
func (s *UserService) DeleteUser(ctx context.Context, req *pb.DeleteUserRequest) (*pb.EmptyReply, error) {
	uid := req.GetUid()
	if _, err := uuid.FromString(uid); err != nil {
		return nil, status.Error(codes.NotFound, "user not found")
	}
	if err := s.DB.Where("uid = ?", req.GetUid()).Delete(&pb.User{}).Error; err != nil {
		return nil, status.Error(codes.Unknown, err.Error())
	}
	return &pb.EmptyReply{Handler: int32(0)}, nil
}

// ListUsers list users
func (s *UserService) ListUsers(ctx context.Context, req *pb.ListUsersRequest) (*pb.UserList, error) {
	count := int32(0)
	users := []*pb.User{}
	query := s.DB.Model(&pb.User{})
	if req.Deleted {
		query = query.Unscoped()
	}
	if err := query.Count(&count).Error; err != nil {
		return nil, status.Error(codes.Unknown, err.Error())
	}
	if err := query.Limit(req.GetLimit()).Offset(req.GetOffset()).Find(&users).Error; err != nil {
		return nil, status.Error(codes.Unknown, err.Error())
	}
	return &pb.UserList{Users: users, Count: count}, nil
}
