package server

import (
	"net"

	log "github.com/sirupsen/logrus"
	pb "gitlab.com/xscalable/users/proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

// Start server
func Start(config Config) {
	// Creates a tcp listener on port :authGrpcPort
	lis, err := net.Listen("tcp", config.GRPCAddr)
	if err != nil {
		log.Fatalf("failed to listen: %v\n", err)
	}

	// Creates a new gRPC server
	grpcServer := grpc.NewServer()

	pb.RegisterUsersServer(grpcServer, NewUsersHandler(config))

	// Register reflection service on gRPC server.
	reflection.Register(grpcServer)

	// start server
	log.Infof("gRPC server listening on %s", config.GRPCAddr)
	grpcServer.Serve(lis)
}
