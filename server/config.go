package server

type Config struct {
	Environment string
	Buildstamp  string
	Githash     string
	Version     string
	GRPCAddr    string
	DBConn      string
}
